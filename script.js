// https://jsonplaceholder.typicode.com/albums
// https://jsonplaceholder.typicode.com/photos
function renderTitle(array) {
    const templateTitle = $('#templateTitle').html()
    const titleContent = $('#titleContent')

    titleContent.empty()

    for (const item of array) {
        let template = templateTitle.replace('__title__', item.title)
        titleContent.append(template)
    }
}

function renderPhoto(title, arrayAlbums, arrayPhotos) {
    const templatePhoto = $('#templatePhoto').html()
    const photoContent = $('#photoContent')

    photoContent.empty()
    let idAlbum
    for (const item of arrayAlbums) {
        if (title === item.title) {
            idAlbum = item.id
            break
        }
    }
    console.log(idAlbum)

    for (const item of arrayPhotos) {
        let template = templatePhoto
        if (idAlbum === item.albumId) {
            template = template.replace('__url__', item.url)
            template = template.replace('__title__', item.title)
            photoContent.append(template)
        }
    }
}

async function title() {
    const requestAlbums = await fetch('https://jsonplaceholder.typicode.com/albums')
    const dataAlbums = await requestAlbums.json()

    renderTitle(dataAlbums) //заповнюємо тайтли альбомів

    const button = $('.btn')
    const requestPhotos = await fetch('https://jsonplaceholder.typicode.com/photos')
    const dataPhotos = await requestPhotos.json()

    button.click(function () {
        renderPhoto($(this).text(), dataAlbums, dataPhotos)
    })// заповюєм по кліку картинки
}

title()

// fetch('https://jsonplaceholder.typicode.com/albums')
//     .then((response) => response.json())
//     .then(data => renderTitle(data))

// const arrayAlbums = $.get('https://jsonplaceholder.typicode.com/albums').done(function (data) {
//     return data
// renderTitle(data)
// const templateTitle = $('#templateTitle').html()
// const titleContent = $('#titleContent')
//
// titleContent.empty()
//
// for (const item of data){
//     let template = templateTitle.replace('__title__',item.title)
//     console.log(template)
//     titleContent.append(template)
// }
// });


